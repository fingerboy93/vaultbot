package main

import (
	"crypto/x509"
	"fmt"
	"os"

	goflags "github.com/jessevdk/go-flags"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msvechla/vaultbot/cli"
)

// VaultbotVersion TODO: bump before release
const VaultbotVersion = "1.9.0"

var options cli.Options

func main() {
	_, err := goflags.ParseArgs(&options, os.Args)

	if err != nil {
		os.Exit(1)
	}

	if options.Version {
		printVersion()
		os.Exit(0)
	}

	setupLogging(options)
	run(options)
}

// printVersion logs the current version and exits
func printVersion() {
	fmt.Printf("Vaultbot v%s\n", VaultbotVersion)
}

// setupLogging configures logarus
func setupLogging(options cli.Options) {
	log.SetFormatter(&log.JSONFormatter{})
	file, err := os.OpenFile(options.Logfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err == nil {
		log.SetOutput(file)
	} else {
		log.Printf("Failed to log to file, using default stderr: %s", err)
	}
}

// run executes all necessary methods in order to execute vaultbot based on specified options
func run(options cli.Options) {
	log.Println("Vaultbot started...")
	if options.PKI.RoleName == "" {
		log.Fatalln("the required flag `--pki_role_name' was not specified!")
	}

	log.Printf("Running Vaultbot v%s", VaultbotVersion)
	client := createClient(options)
	if options.Vault.RenewToken {
		renewSelf(client, options)
	}

	var writeConfirmed = true
	var dueForRenewal = true

	var currentPKICert, currentJKSCert *x509.Certificate

	if options.PKI.JKSExport {
		log.Errorf("Option `pki_jks_export` is deprecated and will be removed in a future release. Exporting to JKS is now determined based on whether `pki_jks_path` has been specified.")
	}

	if options.PKI.JKSPath != "" {
		currentJKSCert = readJKSCurrentCertificate(options)
	}

	if options.PKI.CertPath != "" {
		currentPKICert = readCurrentCertificate(options)
	}

	equal, currentCert := certificatesEqualOrInitial(currentPKICert, currentJKSCert)

	if !equal {
		log.Fatalf("Both pki_cert_path and pky_jks_path have been specified, but the parsed certificates are not equal. Aborting.")
	}

	if currentCert != nil {
		dueForRenewal = isCertificateRenewalDue(currentCert, options)

		if dueForRenewal {
			if hasCertificateDataChanged(currentCert, options) {
				if !options.AutoConfirm {
					writeConfirmed = userConfirmation("Requested certificate data does not match existing certificate, continue anyways?")
				} else {
					log.Println("Requested certificate data does not match existing certificate, continuing anyways (auto confirmed)")
				}
			}
		}
	} else {
		log.Println("No existing certificate found, initial request.")
	}

	if writeConfirmed {
		if dueForRenewal {
			parsedCertBundle := requestCertificate(client, options)
			if options.PKI.JKSPath != "" {
				writeJKSCertificateData(parsedCertBundle, options)
			}

			if options.PKI.CertPath != "" {
				writeCertificateData(parsedCertBundle, options)
			}

			if options.RenewHook != "" {
				executeRenewHook(options.RenewHook)
			}

			log.Println("Certificate renewal finished successfully.")
		}
	} else {
		log.Println("Certificate renewal cancled by user.")
	}

	log.Println("Vaultbot finished successfully.")

}
